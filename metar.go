package main

import (
    "strconv"
    "bufio"
    "fmt"
    "log"
    "os"
    "net/http"
    "unicode"
)

func main() {
	temperature, pressure := metar("EBAW")
        fmt.Println("T=" + strconv.Itoa(temperature) + "°C P=" + strconv.Itoa(pressure) + "hPa")
}

func metar(airport string) (int, int) {
    counter, temperature, pressure := 0, 0, 0

    response, err :=
    http.Get("https://tgftp.nws.noaa.gov/data/observations/metar/stations/" + airport + ".TXT")

    if err != nil {
        fmt.Print(err.Error())
        os.Exit(1)
    }

    scanner := bufio.NewScanner(response.Body)
    for scanner.Scan() {
        if counter == 1 {
            for i := 0; i < len(scanner.Text()) - 4; i++ {
                c  := rune(scanner.Text()[i])
                c1 := rune(scanner.Text()[i + 1])
                c2 := rune(scanner.Text()[i + 2])
                c3 := rune(scanner.Text()[i + 3])
                c4 := rune(scanner.Text()[i + 4])
                if (c == ' ' || c == 'M') && unicode.IsDigit(c1) && unicode.IsDigit(c2) && c3 == '/' {
			temperature = int((c1-48)*10 + c2-48)
			if c == 'M' {
				temperature = temperature * -1
			}
                }
                if ((c == 'Q' || c == 'A') && unicode.IsDigit(c1) && unicode.IsDigit(c2) && unicode.IsDigit(c3) && unicode.IsDigit(c4)) {
                         pressure = int((c1-48)*1000 + (c2-48)*100 + (c3-48)*10 + c4-48);
                         if (c == 'A') {
			      p := float64(pressure) * 1.3333 * 25.4 / 100 // hg -> hectopascal / inch -> mm
			      pressure = int(p)
			 }
		}
            }
	}
        counter++
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    return temperature, pressure
}

